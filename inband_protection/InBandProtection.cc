#include "InBandProtection.hh"

//get uint32_t ip from string
inline uint32_t get_int_ip(std::string str) {
    return fluid_msg::IPAddress::IPv4from_string(str);
}

//convert uint32_t ip into libfluid
inline fluid_msg::IPAddress make_ip(uint32_t ip) {
    return fluid_msg::IPAddress(ip);
}

REGISTER_APPLICATION(InBandProtection, {"link-discovery", "switch-manager", ""})

void InBandProtection::init(Loader *loader, const Config& config) {
    QObject* ld;

    ld = ILinkDiscovery::get(loader);
    switch_manager = SwitchManager::get(loader);
    controller = Controller::get(loader);
    transaction = controller->registerStaticTransaction(this);
    is_rules_installed = false;

    QObject::connect(ld, SIGNAL(linkDiscovered(switch_and_port, switch_and_port)),
                     this, SLOT(linkDiscovered(switch_and_port, switch_and_port)));
    QObject::connect(ld, SIGNAL(linkBroken(switch_and_port, switch_and_port)),
                     this, SLOT(linkBroken(switch_and_port, switch_and_port)));

    readConfig(config);

    // init timer
    renumbering_timer = new QTimer(this);
    renumbering_timer->setInterval(TOPOLOGY_CHANGED_TIME);
    renumbering_timer->setSingleShot(true);
    connect(renumbering_timer, SIGNAL(timeout()), SLOT(timerAction()));
    renumbering_timer->start();
    
    LOG(INFO) << "In Band protection ready";
}

InBandProtection::InBandProtection() {}
InBandProtection::~InBandProtection() {
    delete(renumbering_timer);
}

// Events
void InBandProtection::linkDiscovered(switch_and_port from, switch_and_port to) {
    QWriteLocker locker(renumbering_mutex);
    uint32_t temp = from.port;
    from.port = to.port;
    to.port = temp;

    switch_adjacent &from_adjacent = sws_adjacent[from.dpid];
    switch_adjacent &to_adjacent = sws_adjacent[to.dpid];

    from_adjacent.push_back(to);
    to_adjacent.push_back(from);

    if (!is_rules_installed) {
        if (renumbering_timer->isActive()) {
            renumbering_timer->stop();
        }
        renumbering_timer->start();
    }
}

void InBandProtection::linkBroken(switch_and_port from, switch_and_port to) {
    QWriteLocker locker(renumbering_mutex); 

    auto &from_adjacent = sws_adjacent[from.dpid];
    auto &to_adjacent = sws_adjacent[to.dpid];
    auto from_sw_it = find(from_adjacent.begin(), from_adjacent.end(), to);
    auto to_sw_it = find(to_adjacent.begin(), to_adjacent.end(), from);

    if (from_sw_it != from_adjacent.end()) {
        from_adjacent.erase(from_sw_it);
        from_adjacent.erase(from_sw_it);
    }

    if (to_sw_it != to_adjacent.end()) {
        to_adjacent.erase(to_sw_it);
    }

    if (false) {
        if (renumbering_timer->isActive()) {
            renumbering_timer->stop();
        }
        renumbering_timer->start();
    }
}

void InBandProtection::timerAction() {
    QWriteLocker locker(renumbering_mutex);
    renumbering();
    fillTables();
    is_rules_installed = true;
    log();
}
// Events END

void InBandProtection::readConfig(const Config &config) {
    auto inBandProtectionConf = config_cd(config, "in-band-protection");
    if (inBandProtectionConf.find("dr") != inBandProtectionConf.end()) {
        for (auto ar_item: inBandProtectionConf.at("dr").array_items()) {
            uint64_t dpid = strtoull(ar_item.string_value().c_str(), NULL, 10);

            ar_vec.push_back(dpid);
        }
    }
}

void InBandProtection::renumbering() {
    std::unordered_map<uint64_t, bool> isCalculate;
    std::vector<uint64_t> for_check_vec;
    std::vector<uint64_t> next_step_check_vec;
    uint32_t current_number = 1;

    // clear previously numbers
    sws_number.clear();
    
    for (auto dpid : ar_vec) {
        for_check_vec.push_back(dpid);
    }
    while (for_check_vec.size()) {
        for (auto dpid : for_check_vec) {
            if (isCalculate[dpid]) {
                continue;
            }
            sws_number[dpid] = current_number;
            isCalculate[dpid] = true;
            current_number++;

            auto adj_v = sws_adjacent[dpid];
            auto it = adj_v.begin();
            auto last = adj_v.end();
            while (it != last) {
                next_step_check_vec.push_back(it->dpid);
                it++;
            }
        }
        for_check_vec.clear();
        for_check_vec = next_step_check_vec;
        next_step_check_vec = std::vector<uint64_t>();
    }

    // sort switches adjacency for new number
    for (auto &sw_adjacent : sws_adjacent) {
        std::sort(sw_adjacent.second.begin(), sw_adjacent.second.end(), [&](switch_and_port first, switch_and_port second) {
            auto first_number = sws_number[first.dpid];
            auto second_number = sws_number[second.dpid];

            return first_number < second_number;
        });
    }

}

void InBandProtection::fillTables() {
    auto switches(switch_manager->switches());

    std::sort(switches.begin(), switches.end(), [&](Switch* first, Switch* second) {
        uint64_t first_number = sws_number[first->id()];
        uint64_t second_number = sws_number[second->id()];

        return first_number < second_number;
    });

    for (auto sw : switches) {
        fillSwitchTables(sw);
    }
}


void InBandProtection::fillSwitchTables(const Switch *sw) {
    auto ofconn = sw->connection();
    auto br = of13::BarrierRequest(IN_BAND_XID);

    // Default rule for table 0

    of13::FlowMod fm;
    fm.add_instruction(new of13::GoToTable(DEFAULT_TABLE_ID));
    sendFlowModMessage(ofconn, fm, 0, of13::OFPFC_ADD, 0);
    transaction->request(ofconn, br);

    fillGroupTable(sw);
    transaction->request(ofconn, br);

    fillInPortClassifierTables(sw);
    fillHistoryClassifierTable(sw);
    fillIsInClassifierTable(sw);
    fillClearTable(sw);
    transaction->request(ofconn, br);

    fillForwardingTable(sw);
    transaction->request(ofconn, br);

//    clearForwardingTable(sw);
}
void InBandProtection::clearForwardingTable(const Switch* sw) {
    LOG(INFO) << "CLEAR " << sw->id();
    auto ofconn = sw->connection();
    of13::FlowMod fm;

    fm.xid(IN_BAND_XID);
    fm.command(of13::OFPFC_DELETE);
    fm.cookie(IN_BAND_COOKIE_DEFAULT);
    fm.cookie_mask(0xFFFF);
    fm.table_id(FORWARDING_TABLE_ID);

    transaction->request(ofconn, fm);
}
void InBandProtection::fillForwardingTable(const Switch* sw) {
    auto ofconn = sw->connection();
    auto br = of13::BarrierRequest(IN_BAND_XID);

//    LOCAL Port forwarding with clear EthDst (for traffic from this switch)
    of13::FlowMod fm_local_port;
    of13::ApplyActions actions_local_port;

    fluid_msg::EthAddress ethMod("00:00:00:00:00:00");
    of13::EthDst* ethMatch = new of13::EthDst(ethMod);
    fm_local_port.add_oxm_field(new of13::InPort(of13::OFPP_LOCAL));

    actions_local_port.add_action(new of13::SetFieldAction(ethMatch));

    fm_local_port.add_instruction(actions_local_port);
    fm_local_port.add_instruction(new of13::GoToTable(CLEAR_TABLE_ID));

    sendFlowModMessage(ofconn, fm_local_port, FORWARDING_TABLE_ID, of13::OFPFC_ADD, IN_BAND_HIGH_PRIORITY + 100);
    transaction->request(ofconn, br);

//  Control traffic forwarding (IP-TCP, ARP) go to table 1
    {
        of13::FlowMod fm_tcp;
        of13::FlowMod fm_arp;
        of13::ApplyActions actions;

        fm_tcp.add_oxm_field(new of13::EthType(IP));
        fm_tcp.add_oxm_field(new of13::IPProto(TCP));
        fm_tcp.add_oxm_field(new of13::IPv4Dst("10.0.0.1"));
        fm_tcp.add_instruction(new of13::GoToTable(CLEAR_TABLE_ID));

        fm_arp.add_oxm_field(new of13::EthType(ARP));
        fm_arp.add_oxm_field(new of13::ARPTPA("10.0.0.1"));
        fm_arp.add_instruction(new of13::GoToTable(CLEAR_TABLE_ID));

        sendFlowModMessage(ofconn, fm_tcp, FORWARDING_TABLE_ID);
        sendFlowModMessage(ofconn, fm_arp, FORWARDING_TABLE_ID);
    }

//    If traffic for the switch then output LOCAL
    {
        of13::FlowMod fm_tcp;
        of13::FlowMod fm_arp;
        of13::ApplyActions actions;

        actions.add_action(new of13::SetFieldAction(new of13::EthDst(sw->port(OVS_LOCAL_PORT).hw_addr())));
        actions.add_action(new of13::OutputAction(of13::OFPP_LOCAL, 0));

        fm_tcp.add_oxm_field(new of13::EthType(IP));
        fm_tcp.add_oxm_field(new of13::IPProto(TCP));
        fm_tcp.add_oxm_field(new of13::IPv4Dst(getSwitchIp(sw)));
        fm_tcp.add_instruction(actions);
//        fm_tcp.add_instruction(new of13::GoToTable(7));

        fm_arp.add_oxm_field(new of13::EthType(ARP));
        fm_arp.add_oxm_field(new of13::ARPTPA(getSwitchIp(sw)));
        fm_arp.add_instruction(actions);
//        fm_arp.add_instruction(new of13::GoToTable(7));

        sendFlowModMessage(ofconn, fm_tcp, FORWARDING_TABLE_ID, of13::OFPFC_ADD, IN_BAND_HIGH_PRIORITY + 100);
        sendFlowModMessage(ofconn, fm_arp, FORWARDING_TABLE_ID, of13::OFPFC_ADD, IN_BAND_HIGH_PRIORITY + 100);
        transaction->request(ofconn, br);
    }

    {
        if (isAr(sw->id())) {
            of13::FlowMod fm;
            of13::ApplyActions actions;
            fluid_msg::EthAddress serviceBitAddress("80:00:00:00:00:00");

            actions.add_action(new of13::SetFieldAction(new of13::EthDst(serviceBitAddress)));

            fm.add_oxm_field(new of13::InPort(1));
            fm.add_instruction(new of13::GoToTable(CLEAR_TABLE_ID));
            fm.add_instruction(actions);

            sendFlowModMessage(ofconn, fm, FORWARDING_TABLE_ID, of13::OFPFC_ADD, IN_BAND_HIGH_PRIORITY + 1);
            transaction->request(ofconn, br);
        }

        // Control trafic forwrading from
        of13::FlowMod fm_tcp;
        of13::FlowMod fm_arp;
        of13::ApplyActions actions;
        fluid_msg::EthAddress serviceBitAddress("80:00:00:00:00:00");

        fm_tcp.add_oxm_field(new of13::EthType(IP));
        fm_tcp.add_oxm_field(new of13::IPProto(TCP));
        fm_tcp.add_oxm_field(new of13::IPv4Src("10.0.0.1"));
        fm_tcp.add_oxm_field(new of13::EthDst(serviceBitAddress, serviceBitAddress));
        fm_tcp.add_instruction(new of13::GoToTable(CLEAR_TABLE_ID));

        fm_arp.add_oxm_field(new of13::EthType(ARP));
        fm_arp.add_oxm_field(new of13::ARPSPA("10.0.0.1"));
        fm_arp.add_oxm_field(new of13::EthDst(serviceBitAddress, serviceBitAddress));
        fm_arp.add_instruction(new of13::GoToTable(CLEAR_TABLE_ID));

        sendFlowModMessage(ofconn, fm_tcp, FORWARDING_TABLE_ID);
        sendFlowModMessage(ofconn, fm_arp, FORWARDING_TABLE_ID);
    }

}
void InBandProtection::fillClearTable(const Switch* sw) {
    uint64_t mask = 0xffffffffffff;
    uint64_t zero_address = 0x0;

    // Fill NOT IN Table
    uint64_t matching_address = 0x100000000000;
    for (uint8_t i = 0; i < N_SECTIONS; ++i) {
        ethaddr matchAddr(matching_address);
        ethaddr matchMask((0x100000000000 | (mask << i * N_BITS_PER_SECTION)) & 0x1fffffffffff);
        ethaddr modAddr(zero_address);
        ethaddr modMask((mask << (i == 0 ? i : (i-1)) * N_BITS_PER_SECTION) & 0x1fffffffffff);

        fluid_msg::EthAddress ethMatchAddress(ethaddrToString(matchAddr));
        fluid_msg::EthAddress ethMatchMask(ethaddrToString(matchMask));
        of13::EthDst* ethMatch = new of13::EthDst(ethMatchAddress, ethMatchMask);

        fluid_msg::EthAddress ethModAddress(ethaddrToString(modAddr));
        fluid_msg::EthAddress ethModMask(ethaddrToString(modMask));
        of13::EthDst* ethMod = new of13::EthDst(ethModAddress, ethModMask);

        OvsCommand *cmd = new OvsCommand(sw->id(), 0);
        cmd->set_table(CLEAR_TABLE_ID)
                ->set_priority(IN_BAND_HIGH_PRIORITY + N_SECTIONS - i)
                ->set_cookie(IN_BAND_COOKIE)
                ->set_match(ethMatch)
                ->set_mod(ethMod)
                ->set_goto_table(IS_IN_CLASSIFIER_TABLE_ID)
                ->run();

        delete(cmd);
    }

    of13::FlowMod fm;
    fm.add_instruction(new of13::GoToTable(IS_IN_CLASSIFIER_TABLE_ID));
    sendFlowModMessage(sw->connection(), fm, CLEAR_TABLE_ID, of13::OFPFC_ADD, 0);
}

void InBandProtection::fillIsInClassifierTable(const Switch* sw) {
    uint64_t sw_num = sw->id();
    uint64_t mask = 0xf;

    // IF In history
    for (uint8_t i = 0; i < N_SECTIONS; ++i) {
        of13::FlowMod fm;
        of13::Match match;

        ethaddr curAddr(sw_num << i * N_BITS_PER_SECTION);
        ethaddr curMask((mask << i * N_BITS_PER_SECTION) & 0x0fffffffffff);

        fluid_msg::EthAddress ethCurAddress(ethaddrToString(curAddr));
        fluid_msg::EthAddress ethCurMask(ethaddrToString(curMask));

        of13::EthDst* ethMatch = new of13::EthDst(ethCurAddress, ethCurMask);
        match.add_oxm_field(ethMatch);

        fm.add_instruction(new of13::GoToTable(HISTORY_CLASSIFIER_IN_TABLE_ID));
        fm.match(match);

        sendFlowModMessage(sw->connection(), fm, IS_IN_CLASSIFIER_TABLE_ID);
    }

    // IF not in history
    of13::FlowMod fm;
    fm.add_instruction(new of13::GoToTable(HISTORY_CLASSIFIER_NOT_IN_TABLE_ID));
    sendFlowModMessage(sw->connection(), fm, IS_IN_CLASSIFIER_TABLE_ID, of13::OFPFC_ADD, 0);
}

void InBandProtection::fillHistoryClassifierTable(const Switch* sw) {
    uint64_t sw_num = sw->id();
    uint64_t mask = 0xffffffffffff;

    // Fill IN Table
    for (uint8_t i = 0; i < N_SECTIONS; ++i) {
        of13::FlowMod fm;
        of13::Match match;

        ethaddr curAddr(sw_num << i * N_BITS_PER_SECTION);
        ethaddr curMask((mask << i * N_BITS_PER_SECTION) & 0x0fffffffffff);

        fluid_msg::EthAddress ethCurAddress(ethaddrToString(curAddr));
        fluid_msg::EthAddress ethCurMask(ethaddrToString(curMask));

        of13::EthDst* ethMatch = new of13::EthDst(ethCurAddress, ethCurMask);
        match.add_oxm_field(ethMatch);

        fm.add_instruction(new of13::GoToTable(IN_PORT_CLASSIFIER_IN_TABLE_ID));
        fm.match(match);

        sendFlowModMessage(sw->connection(), fm, HISTORY_CLASSIFIER_IN_TABLE_ID,
                           of13::OFPFC_ADD, IN_BAND_HIGH_PRIORITY + N_SECTIONS - i);
    }

//    fluid_msg::EthAddress ethAddress("10:00:00:00:00:00");
//    fluid_msg::EthAddress ethMask("10:00:00:00:00:00");
//    of13::EthDst* ethSet = new of13::EthDst(ethAddress, ethMask);

    OvsCommand *cmd = new OvsCommand(sw->id(), 0);
    cmd->set_table(HISTORY_CLASSIFIER_IN_TABLE_ID)
            ->set_priority(0)
            ->set_cookie(IN_BAND_COOKIE)
            ->set_output(36000)
            ->run();

    delete(cmd);


    // Fill NOT IN Table
    uint64_t zero_address = 0;
    for (uint8_t i = 0; i < N_SECTIONS; ++i) {
        ethaddr matchAddr(zero_address);
        ethaddr matchMask((mask << i * N_BITS_PER_SECTION) & 0x0fffffffffff);
        ethaddr modAddr(sw_num << i * N_BITS_PER_SECTION);
        ethaddr modMask((mask << i * N_BITS_PER_SECTION) & 0x0fffffffffff);

        fluid_msg::EthAddress ethMatchAddress(ethaddrToString(matchAddr));
        fluid_msg::EthAddress ethMatchMask(ethaddrToString(matchMask));
        of13::EthDst* ethMatch = new of13::EthDst(ethMatchAddress, ethMatchMask);

        fluid_msg::EthAddress ethModAddress(ethaddrToString(modAddr));
        fluid_msg::EthAddress ethModMask(ethaddrToString(modMask));
        of13::EthDst* ethMod = new of13::EthDst(ethModAddress, ethModMask);

        OvsCommand *cmd = new OvsCommand(sw->id(), 0);
        cmd->set_table(HISTORY_CLASSIFIER_NOT_IN_TABLE_ID)
                ->set_priority(IN_BAND_HIGH_PRIORITY + N_SECTIONS - i)
                ->set_cookie(IN_BAND_COOKIE)
                ->set_match(ethMatch)
                ->set_mod(ethMod)
                ->set_goto_table(IN_PORT_CLASSIFIER_NOT_IN_TABLE_ID)
                ->run();

        delete(cmd);
    }

    of13::FlowMod fm;
    sendFlowModMessage(sw->connection(), fm, HISTORY_CLASSIFIER_NOT_IN_TABLE_ID, of13::OFPFC_ADD, 0);
}

void InBandProtection::fillInPortClassifierTables(const Switch *sw) {

    for (auto port : sw->ports()) {
        if (port.port_no() == OVS_LOCAL_PORT) {
            continue;
        }
        of13::FlowMod fm;

        of13::Match m;
        m.add_oxm_field(new of13::InPort(port.port_no()));

        of13::ApplyActions actions;
        actions.add_action(new of13::GroupAction(START_IN_GROUP_TO_CONTROLLER + port.port_no()));

        fm.add_instruction(actions);
        fm.match(m);

        sendFlowModMessage(sw->connection(), fm, IN_PORT_CLASSIFIER_IN_TABLE_ID);

//        TODO make pretty
        of13::FlowMod fm_from;
        of13::Match m_from;
        of13::ApplyActions actions_from;
        fluid_msg::EthAddress serviceBitAddress("80:00:00:00:00:00");

        m_from.add_oxm_field(new of13::InPort(port.port_no()));
        m_from.add_oxm_field(new of13::EthDst(serviceBitAddress, serviceBitAddress));
        actions_from.add_action(new of13::GroupAction(START_IN_GROUP_FROM_CONTROLLER + port.port_no()));

        fm_from.add_instruction(actions_from);
        fm_from.match(m_from);

        sendFlowModMessage(sw->connection(), fm_from,
                           IN_PORT_CLASSIFIER_IN_TABLE_ID,
                           of13::OFPFC_ADD, IN_BAND_HIGH_PRIORITY + 1);
    }

    for (auto port : sw->ports()) {
        if (port.port_no() == OVS_LOCAL_PORT) {
            continue;
        }
        of13::FlowMod fm;

        of13::Match m;
        m.add_oxm_field(new of13::InPort(port.port_no()));

        of13::ApplyActions actions;
        actions.add_action(new of13::GroupAction(START_NOT_IN_GROUP_TO_CONTROLLER + port.port_no()));

        fm.add_instruction(actions);
        fm.match(m);

        sendFlowModMessage(sw->connection(), fm, IN_PORT_CLASSIFIER_NOT_IN_TABLE_ID);

//        TODO make pretty
        of13::FlowMod fm_from;
        of13::Match m_from;
        of13::ApplyActions actions_from;
        fluid_msg::EthAddress serviceBitAddress("80:00:00:00:00:00");

        m_from.add_oxm_field(new of13::InPort(port.port_no()));
        m_from.add_oxm_field(new of13::EthDst(serviceBitAddress, serviceBitAddress));
        actions_from.add_action(new of13::GroupAction(START_NOT_IN_GROUP_FROM_CONTROLLER + port.port_no()));

        fm_from.add_instruction(actions_from);
        fm_from.match(m_from);

        sendFlowModMessage(sw->connection(), fm_from,
                           IN_PORT_CLASSIFIER_NOT_IN_TABLE_ID,
                           of13::OFPFC_ADD, IN_BAND_HIGH_PRIORITY + 1);
    }

//  for LOCAL port
    of13::FlowMod fm;

    of13::Match m;
    m.add_oxm_field(new of13::InPort(of13::OFPP_LOCAL));

    of13::ApplyActions actions;
    actions.add_action(new of13::GroupAction(START_NOT_IN_GROUP_TO_CONTROLLER + 36));

    fm.add_instruction(actions);
    fm.match(m);

    sendFlowModMessage(sw->connection(), fm, IN_PORT_CLASSIFIER_NOT_IN_TABLE_ID);
}

void InBandProtection::fillGroupTable(const Switch* sw) {
    auto dpid = sw->id();

    switch_adjacent sw_adjacent = sws_adjacent[dpid];
    switch_adjacent sw_adjacent_reverse;

    sw_adjacent_reverse.resize(sw_adjacent.size());
    std::reverse_copy(sw_adjacent.begin(), sw_adjacent.end(), sw_adjacent_reverse.begin());

//    Fill to controller (normal ordering)
    fillGroupTableWithSortedAdjacency(sw, sw_adjacent, true);
//    Fill from controller (reverse ordering)
    fillGroupTableWithSortedAdjacency(sw, sw_adjacent_reverse, false);
}

void InBandProtection::fillGroupTableWithSortedAdjacency(const Switch* sw,
                                                         const switch_adjacent& sw_adjacent,
                                                         bool isToController) {

    std::vector<uint32_t> current_switch_ports;
    uint32_t start_not_in_group = isToController ?
                                  START_NOT_IN_GROUP_TO_CONTROLLER :
                                  START_NOT_IN_GROUP_FROM_CONTROLLER;
    uint32_t start_in_group = isToController ?
                              START_IN_GROUP_TO_CONTROLLER :
                              START_IN_GROUP_FROM_CONTROLLER;

    // init all switch ports vector
    for (auto sw_port : sw_adjacent) {
        current_switch_ports.push_back(sw_port.port);
    }

    // for localport
    if (isToController) {
        sendOvsMessage(sw->id(), start_not_in_group + 36,
                       current_switch_ports,
                       of13::OFPP_LOCAL);
    }

    // send not in group entity
    for (auto port_no : current_switch_ports) {
        // TODO: for of15 use sendFastFailoverGroupEntity
        sendOvsMessage(sw->id(), start_not_in_group + port_no,
                       current_switch_ports,
                       port_no, isToController);
    }

    // send in group entity
    for (uint32_t i = 0; i < current_switch_ports.size(); ++i) {
        std::vector<uint32_t> current_ports(current_switch_ports.begin() + i, current_switch_ports.end());
        // TODO: for of15 use sendFastFailoverGroupEntity
        sendOvsMessage(sw->id(), start_in_group + current_switch_ports[i],
                       current_ports,
                       current_switch_ports[i], isToController);
    }


    // if is ar and moved from controller add group for controller port
    if (!isToController && isAr(sw->id())) {
        LOG(INFO) << "ARR FROM CONTROLLER: " << sw->id() << " " << start_not_in_group + 1;
        sendOvsMessage(sw->id(), start_not_in_group + 1,
                       current_switch_ports,
                       1, isToController);

        sendOvsMessage(sw->id(), start_in_group + 1,
                       current_switch_ports,
                       1, isToController);
    }
}

// For of15 only (masked set-field)
void InBandProtection::sendFastFailoverGroupEntity(const SwitchConnectionPtr &ofconn,
                                                   uint32_t group_number,
                                                   const std::vector<uint32_t> &ports,
                                                   uint32_t in_port,
                                                   uint8_t COMMAND) {

    of13::GroupMod gm;
    gm.commmand(COMMAND);
    gm.xid(IN_BAND_XID);
    gm.group_type(fluid_msg::of13::OFPGT_FF);
    gm.group_id(group_number);

    for (auto port : ports) {
        if (port == in_port || port == of13::OFPP_LOCAL) continue;

        of13::Bucket b;
        b.add_action(new of13::OutputAction(port, 0));
        b.watch_port(port);
        gm.add_bucket(b);
    }

    // last bucket to in_port with clear bit
    of13::Bucket b;
    fluid_msg::EthAddress ethAddress("10:00:00:00:00:00");
    fluid_msg::EthAddress ethMask("10:00:00:00:00:00");

    of13::EthDst* ethDst = new of13::EthDst(ethAddress, ethMask);
    b.add_action(new of13::OutputAction(in_port, 0));
    b.add_action(new of13::SetFieldAction(ethDst));
    b.watch_port(in_port);
    gm.add_bucket(b);

    transaction->request(ofconn, gm);
}

void InBandProtection::sendFlowModMessage(const SwitchConnectionPtr &ofconn,
                        of13::FlowMod& fm,
                        uint8_t table_id,
                        uint8_t COMMAND,
                        uint16_t priority) {

    fm.xid(IN_BAND_XID);
    fm.priority(priority);
    fm.command(COMMAND);
    fm.cookie(IN_BAND_COOKIE);
    fm.table_id(table_id);

    transaction->request(ofconn, fm);
}

// todo: deprecated. use OvsMessage instead
void InBandProtection::sendOvsMessage(uint64_t dpid,
                                      uint32_t group_number,
                                      const std::vector<uint32_t> &ports,
                                      uint32_t in_port,
                                      bool isToController) {

    std::ostringstream cmd;
    uint32_t res;

    cmd << "ovs-ofctl -O OpenFlow13 add-group s" << dpid << " \"group_id=" << group_number << ",type=ff";

    if (isToController && isAr(dpid)) {
        // try reach controller
        cmd << ",bucket=" << "watch_port:1,output:1,set_field:00:00:00:00:00:aa->eth_dst";
    }

    for (auto port : ports) {
        if (port == in_port || port == of13::OFPP_LOCAL ||
            (!isToController && isAr(dpid) && port == 1)) continue;

        cmd << ",bucket=" << "watch_port:" << port << ",output:" << port;
    }

    if (in_port != of13::OFPP_LOCAL) {
        // last bucket to in_port with clear bit (0100)
        cmd << ",bucket=" << "watch_port:" << in_port
            << ",set_field:10:00:00:00:00:00/10:00:00:00:00:00->eth_dst" << ",in_port";
    }

    cmd << "\"";

    if (dpid == 2) LOG(INFO) << cmd.str();

    res = system(cmd.str().c_str());
    if (res != 0) {
        LOG(INFO) << "Ovs add-group error: " << cmd.str();
    }
}

// HELPERS
bool InBandProtection::isAr(uint64_t dpid) {
    return std::find(ar_vec.begin(), ar_vec.end(), dpid) != ar_vec.end();
}
std::string InBandProtection::ethaddrToString(ethaddr addr) {
    std::ostringstream stringStream;
    stringStream << addr;
    return stringStream.str();
}
std::string InBandProtection::getSwitchIp(const Switch* sw) {
    SwitchConnectionPtr conn = sw->connection();
    std::string address = conn->get_peer_address();
    char *cstr = new char[address.length() + 1];
    strcpy(cstr, address.c_str());

    return std::string(strtok(cstr, ":"));
}
void InBandProtection::log() {
    LOG(INFO) << "Adjacents: ";
    for (auto it_sw_adj = sws_adjacent.begin(); it_sw_adj != sws_adjacent.end(); it_sw_adj++) {
        LOG(INFO) << "For: " << it_sw_adj->first;
        for (auto it_sw = it_sw_adj->second.begin(); it_sw != it_sw_adj->second.end(); it_sw++) {
            LOG(INFO) << "s: " << it_sw->dpid << " p: " << it_sw->port;
        }
    }

    LOG(INFO) << "Numbers: ";
    for (auto it_sw = sws_number.begin(); it_sw != sws_number.end(); it_sw++) {
        LOG(INFO) << "s: " << it_sw->first << " n: " << it_sw->second;
    }
}


// OvsCommand
OvsCommand::OvsCommand(uint64_t dpid, uint8_t _type) {
    type = _type;
    switch (_type) {
        case 1:
            cmd << "ovs-ofctl -O OpenFlow13 add-group s" << dpid << " \"";
            break;
        default:
            cmd << "ovs-ofctl -O OpenFlow13 add-flow s" << dpid << " \"";
            break;
    }
}

OvsCommand* OvsCommand::set_output(uint32_t output) {
    std::ostringstream str;
    str << "output=";
    if (output == 36000) {
        str << "in_port";
    } else {
        str << output;
    }
    actions.push_back(str.str());
    return this;
}
OvsCommand* OvsCommand::set_table(uint16_t table) {
    cmd << "table=" << table;
    return this;
}
OvsCommand* OvsCommand::set_group(uint16_t group, const char* type) {
    cmd << "group_id=" << group << ",type=" << type;
    return this;
}
OvsCommand* OvsCommand::set_priority(uint16_t priority) {
    cmd << ",priority=" << priority;
    return this;
}
OvsCommand* OvsCommand::set_goto_table(uint16_t table) {
    std::ostringstream str;
    str << "goto_table:" << table;
    actions.push_back(str.str());
    return this;
}
OvsCommand* OvsCommand::set_cookie(uint64_t cookie) {
    cmd << ",cookie=" << cookie;
    return this;
}
OvsCommand* OvsCommand::set_match(of13::EthDst *ethMatch) {
    cmd << ",dl_dst=" <<  ethMatch->value().to_string() << "/" << ethMatch->mask().to_string();
    return this;
}
OvsCommand* OvsCommand::set_mod(of13::EthDst *ethMod) {
    std::ostringstream str;
    str << "set_field:" << ethMod->value().to_string() << "/" << ethMod->mask().to_string()
                        << "->eth_dst";
    actions.push_back(str.str());
    return this;
}
OvsCommand* OvsCommand::add_bucket(uint32_t watch_port, uint32_t output) {
    cmd << ",bucket=" << "watch_port:" << watch_port << ",output:" << output;

    return this;
}
OvsCommand* OvsCommand::add_bucket(uint32_t watch_port, uint32_t output, of13::EthDst *ethMod) {
    cmd << ",bucket=" << "watch_port:" << watch_port << ",output:" << output
        << ",set_field:" << ethMod->value().to_string() << "/" << ethMod->mask().to_string()
                         << "->eth_dst";

    return this;
}
int OvsCommand::run() {
    std::ostringstream cur_cmd;
    int system_code;

    cur_cmd << cmd.str();
    if (actions.size() > 0) {
        cur_cmd << ",actions=";
        for (uint8_t i = 0; i < actions.size(); ++i) {
            cur_cmd << actions[i];
            if (i < actions.size() - 1) {
                cur_cmd << ",";
            }
        }
    }
    cur_cmd << "\"";

    system_code = system(cur_cmd.str().c_str());
    if (system_code != 0) {
        LOG(INFO) << "OvsCommand error(" << system_code << "): " << cur_cmd.str();
    }
    return system_code;
}

/*
void InBandProtection::in_band_protection() {
    switches_adjacent sws_adjacent;
    switches_number sws_number;
    sws_adjacent = _topo->getSwitchesAdjacent();
    sws_number = _topo->getSwitchesNumber();
    for (auto sw_adjacent : sws_adjacent) {
        std::sort(sw_adjacent.second.begin(), sw_adjacent.second.end(), [&](switch_and_port first, switch_and_port second) {
            return sws_number[first.dpid] < sws_number[second.dpid];
        });
    }
    for (auto sw : switch_manager->switches()) {
        std::vector<uint32_t> ports;
        for (auto sw_p : sws_adjacent[sw->dpid()]) {
            ports.push_back(sw_p.port);
        }
        add_protection_ff_group(sw->dpid(), ports, ports[0]);
    }
}

uint32_t InBandProtection::add_protection_ff_group(uint64_t dpid, std::vector<uint32_t> ports, uint32_t in_port) {
    of13::GroupMod gm;
    gm.commmand(fluid_msg::of13::OFPGC_ADD);
    gm.group_type(fluid_msg::of13::OFPGT_FF);
    auto gr_id = get_new_protection_group_id(dpid);
    gm.group_id(gr_id);

    for (auto port : ports) {
        of13::Bucket b;
        b.add_action(new of13::SetQueueAction(IN_BAND_QUEUE));
        b.add_action(new of13::OutputAction(port, 0));
        b.watch_group(fluid_msg::of13::OFPG_ANY);
        b.watch_port(port);
        gm.add_bucket(b);
    }

    of13::Bucket last_b;
    last_b.add_action(new of13::SetQueueAction(IN_BAND_QUEUE));
    last_b.add_action(new of13::OutputAction(of13::OFPP_IN_PORT, 0));
    last_b.watch_group(fluid_msg::of13::OFPG_ANY);
    last_b.watch_port(in_port);
    gm.add_bucket(last_b);

    switch_manager->switch_(dpid)->connection()->send(gm);
    return gr_id;
}

void InBandProtection::add_group(uint64_t cookie, uint64_t dpid, uint32_t group_id) {
    if (groups.count(cookie)) {
        if (groups[cookie].count(dpid)) {
            groups[cookie][dpid].emplace_back(group_id);
        } else {
            groups[cookie][dpid] = GroupsList{group_id};
        }
    } else {
        groups[cookie] = SwitchGroups{{dpid, {group_id}}};
    }
}


switches_adjacent InBandProtection::getSwitchesAdjacent() {
    switches_adjacent sws_adjacent;
    for (auto vertex_pair : vertex_map) {
        switch_adjacent sw_adjacent;
        auto adjacentes = adjacent_vertices(vertex_pair.second, graph);
        auto it = adjacentes.first;
        auto last = adjacentes.second;
        auto cur_v = vertex(vertex_pair.first);
        while (it != last) {
            auto found_edge = edge(cur_v, *it, graph);
            link_property link = graph[found_edge.first];
            sw_adjacent.push_back(link.target);
            it++;
        }
        sws_adjacent.insert(std::make_pair<uint64_t, switch_adjacent>(
                                (uint64_t)vertex_pair.first, (switch_adjacent)sw_adjacent));
    }
}

switches_number InBandProtection::getSwitchesNumber() {
    
}
//*/
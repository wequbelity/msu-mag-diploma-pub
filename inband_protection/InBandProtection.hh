/** @file */
#pragma once

#include <QTimer>
#include <algorithm>

#include "Application.hh"
#include "Loader.hh"
#include "Common.hh"
#include "Topology.hh"
#include "ILinkDiscovery.hh"
#include "Switch.hh"
#include "Controller.hh"
#include "OFTransaction.hh"
#include "SwitchConnection.hh"
#include "types/ethaddr.hh"


namespace inband_protection {
    #define FORWARDING_TABLE_ID 0
    #define CLEAR_TABLE_ID 1
    #define IS_IN_CLASSIFIER_TABLE_ID 2
    #define HISTORY_CLASSIFIER_IN_TABLE_ID 3
    #define IN_PORT_CLASSIFIER_IN_TABLE_ID 4
    #define HISTORY_CLASSIFIER_NOT_IN_TABLE_ID 5
    #define IN_PORT_CLASSIFIER_NOT_IN_TABLE_ID 6
    #define DEFAULT_TABLE_ID 7
    #define LAST_TABLE_ID 9

    #define START_IN_GROUP_TO_CONTROLLER 1000
    #define START_NOT_IN_GROUP_TO_CONTROLLER 1100
    #define START_IN_GROUP_FROM_CONTROLLER 1200
    #define START_NOT_IN_GROUP_FROM_CONTROLLER 1300

    #define TOPOLOGY_CHANGED_TIME 6000
    #define IN_BAND_XID 4000
    #define IN_BAND_COOKIE 0xAAAA
    #define IN_BAND_COOKIE_DEFAULT 0xDEFA

    #define IN_BAND_HIGH_PRIORITY 60001

    #define N_SECTIONS 11
    #define N_BITS_PER_SECTION 4

    #define OVS_LOCAL_PORT 4294967294

    #define ARP 0x0806
    #define IP 0x0800
    #define TCP 0x06

    typedef std::vector<switch_and_port> switch_adjacent;
    typedef std::unordered_map<uint64_t, switch_adjacent> switches_adjacent;
    typedef std::unordered_map<uint64_t, uint64_t> switches_number;
    typedef std::unordered_map<uint64_t, std::vector<uint64_t> > switches_order;

    struct OvsCommand {

        // todo make partials
        std::ostringstream cmd;
        std::vector<std::string> actions;
        uint8_t type;

        OvsCommand(uint64_t dpid, uint8_t type);

        OvsCommand* set_output(uint32_t output);
        OvsCommand* set_table(uint16_t table);
        OvsCommand* set_cookie(uint64_t cookie);
        OvsCommand* set_group(uint16_t group, const char* type);
        OvsCommand* set_goto_table(uint16_t table);
        OvsCommand* set_match(of13::EthDst *ethMatch);
        OvsCommand* set_mod(of13::EthDst *ethMod);
        OvsCommand* set_priority(uint16_t priority);
        OvsCommand* add_bucket(uint32_t watch_port, uint32_t output);
        OvsCommand* add_bucket(uint32_t watch_port, uint32_t output, of13::EthDst *ethMod);

        int run();
    };
}

using namespace inband_protection;

class InBandProtection : public Application {
    Q_OBJECT
    SIMPLE_APPLICATION(InBandProtection, "in-band-protection")

public:
    InBandProtection();
    ~InBandProtection();

    void init(Loader* loader, const Config&) override;

protected slots:
    void linkDiscovered(switch_and_port from, switch_and_port to);
    void linkBroken(switch_and_port from, switch_and_port to);
    void timerAction();

private:
    Controller* controller;
    SwitchManager* switch_manager;
    QReadWriteLock* renumbering_mutex;
    OFTransaction *transaction;
    QTimer* renumbering_timer;

    switches_adjacent sws_adjacent;
    switches_number sws_number;
    std::vector<uint64_t> ar_vec;

    bool is_rules_installed;

    void readConfig(const Config &config);
    void renumbering();

    void fillTables();

    void fillSwitchTables(const Switch* sw);
    void fillGroupTable(const Switch* sw);
    void fillInPortClassifierTables(const Switch *sw);
    void fillHistoryClassifierTable(const Switch* sw);
    void fillIsInClassifierTable(const Switch* sw);
    void fillClearTable(const Switch* sw);
    void fillForwardingTable(const Switch* sw);
    void clearForwardingTable(const Switch* sw);

    void fillGroupTableWithSortedAdjacency(const Switch* sw,
                                           const switch_adjacent& sw_adjacent,
                                           bool isToController);




    //----
    void sendFastFailoverGroupEntity(const SwitchConnectionPtr &ofconn,
                                     uint32_t start_group_number,
                                     const std::vector<uint32_t> &ports,
                                     uint32_t in_port,
                                     uint8_t COMMAND = fluid_msg::of13::OFPGC_ADD);

    void sendFlowModMessage(const SwitchConnectionPtr &ofconn,
                            of13::FlowMod& fm,
                            uint8_t table_id,
                            uint8_t COMMAND = of13::OFPFC_ADD,
                            uint16_t priority = IN_BAND_HIGH_PRIORITY);


    //----
    void sendOvsMessage(uint64_t dpid,
                        uint16_t table_id,
                        of13::EthDst* ethDst,
                        of13::EthDst* ethSet,
                        uint16_t priority = IN_BAND_HIGH_PRIORITY,
                        uint16_t goto_table_id = 256);

    void sendOvsMessage(uint64_t dpid,
                        uint32_t group_number,
                        const std::vector<uint32_t> &ports,
                        uint32_t in_port,
                        bool isToController = true);


    //// Helpers
    std::string getSwitchIp(const Switch* sw);
    std::string ethaddrToString(ethaddr addr);
    bool isAr(uint64_t dpid);
    void log();
};



#!/usr/bin/python

from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.node import RemoteController, OVSSwitch
from subprocess import call
from collections import *
import json
import sys


class Node():

    n_edge = 0
    def __init__(self, sw, id, name, ip):
        self.sw = sw
        self.id = id
        self.name = name
        self.ip = ip
        self.neighbors = defaultdict()
        self.is_default_rules = False
        self.parent = None

    def add_neighbor(self, neighbor_node):
        self.n_edge += 1
        self.neighbors[neighbor_node] = self.n_edge

class Network():

    currentNodeIp = 101
    maxNodeIp = 254

    def __init__(self, mininet):
        self.nodes = defaultdict()
        self.edges = defaultdict()
        self.mininet = mininet

    def set_controller_node(self,  name, controller_ip):
        controller_node = self.mininet.addHost(name, ip=controller_ip)
        controller_ar = self.mininet.addSwitch('s100', cls=OVSSwitch)
        self.mininet.addLink(controller_node, controller_ar)
        controller_node.setMAC('00:00:00:00:00:aa', 'controller-eth0')        
        self.controller_node = controller_ar
        self.controller_ip = controller_ip

    def add_node(self, id):
        if (self.currentNodeIp > self.maxNodeIp):
            print 'Error: cant create node'
            return
        ip = '10.0.0.' + str(self.currentNodeIp)
        name = 's' + str(id)
        self.currentNodeIp += 1

        switch = self.mininet.addHost(name, ip='0.0.0.0')


        node = Node(switch, id, name, ip)
        self.nodes[id] = node

    def add_edge(self, from_id, to_id):
        to_node = self.nodes[to_id]
        from_node = self.nodes[from_id]
        self.mininet.addLink(to_node.sw, from_node.sw)
        to_node.add_neighbor(from_node)
        from_node.add_neighbor(to_node)


    def clear(self):
        for node in self.nodes.values():
            node.sw.cmd('ovs-vsctl --db=unix:/tmp/mininet-{0}/db.sock del-br {0}'.format(node.name))
        call("ps -ef | grep mininet- | grep -v grep | awk '{print $2}' | xargs sudo kill", shell=True)

    def configure(self):
        for node in self.nodes.values():
            self.__configure_node(node)

        self.__set_default_rules()

        pass

    def get_node(self, id):
        return self.nodes[id]

    def read_data(self, nodes, edges, out_of_band_nodes):
        self._out_of_band_nodes = out_of_band_nodes
        self._nodes = nodes
        self._edges = edges
        for node_id in nodes:
            self.add_node(node_id)

        for out_of_band_node_id in out_of_band_nodes:
            print 'link: controller to ' + str(self.nodes[out_of_band_node_id].name)
            self.mininet.addLink(self.controller_node,
                                 self.nodes[out_of_band_node_id].sw)
            self.nodes[out_of_band_node_id].add_neighbor(None)

        for edge in edges:
            self.add_edge(edge['to'], edge['from'])



    def __configure_node(self, node):
        print('00:00:00:00:00:{:02x}'.format(node.id))
        switch = node.sw
        switch.cmd('bash ./startOvsDb ' + node.name)
        switch.cmd('bash ./startOvs ' + node.name)
        switch.cmd('bash ./createBrSdn ' + node.name + ' ' + ('00:00:00:00:00:{:02x}'.format(node.id)))
        for i in range(node.n_edge):
            switch.cmd('ovs-vsctl --db=unix:/tmp/mininet-{0}/db.sock add-port {0} {0}-eth{1}'.format(node.name, i))
        switch.cmd('ifconfig ' + node.name + ' inet ' + node.ip + '/8')

    def __set_default_rules(self):
        for i in self._out_of_band_nodes:
            node = self.get_node(i)
            self.__set_default_rules_to_node(node, 1, True)

        for i in self._out_of_band_nodes:
            node = self.get_node(i)
            self.__set_default_in_band_rules(node)


    def __set_default_in_band_rules(self, node):
        for to_node in node.neighbors.keys():
            if to_node is None or to_node.is_default_rules:
                continue
            to_node.parent = node
            self.__set_default_rules_to_node(to_node, to_node.neighbors[node])
            self.__set_default_in_band_rules(to_node)

    def __set_default_rules_to_node(self, node, port_to_controller, isOutOfBand=False):
        print 'default rules to node {0} with controller on {1}'.format(node.name, port_to_controller)
        node.is_default_rules = True
        if (isOutOfBand):
            print 'ovs-ofctl add-flow {0} cookie=0xDEFA,priority=60000,dl_type=0x0806,nw_dst={1},actions="output:{2},set_field:{3}->eth_dst"'.format(
                            node.name, self.controller_ip, port_to_controller, '00:00:00:00:00:aa')
            node.sw.cmd('ovs-ofctl add-flow {0} cookie=0xDEFA,priority=60000,dl_type=0x0806,nw_dst={1},actions="output:{2},set_field:{3}->eth_dst"'.format(
                            node.name, self.controller_ip, port_to_controller, '00:00:00:00:00:aa'))
            node.sw.cmd('ovs-ofctl add-flow {0} cookie=0xDEFA,priority=60000,dl_type=0x0800,nw_dst={1},actions="output:{2},set_field:{3}->eth_dst"'.format(
                            node.name, self.controller_ip, port_to_controller, '00:00:00:00:00:aa'))
            node.sw.cmd('ovs-ofctl add-flow {0} cookie=0xDEFA,priority=64000,dl_type=0x86dd,ipv6_dst=ff02::fb,actions="drop"'.format(node.name))
            print 'ovs-ofctl add-flow {0} cookie=0xDEFA,priority=64000,dl_type=0x86dd,ipv6_dst=ff02::fb,actions="drop"'.format(node.name)
        else:
            node.sw.cmd('ovs-ofctl add-flow {0} cookie=0xDEFA,priority=60000,dl_type=0x0806,nw_dst={1},actions=output:{2}'.format(node.name, self.controller_ip, port_to_controller))
            node.sw.cmd('ovs-ofctl add-flow {0} cookie=0xDEFA,priority=60000,dl_type=0x0800,nw_dst={1},actions=output:{2}'.format(node.name, self.controller_ip, port_to_controller))
        node.sw.cmd('ovs-ofctl add-flow {0} cookie=0xDEFA,priority=60000,dl_type=0x0806,nw_src={1},nw_dst={2},actions="set_field:{3}->eth_dst,output:local"'.format(
            node.name, self.controller_ip, node.ip, '00:00:00:00:00:{:02x}'.format(node.id)))
        node.sw.cmd('ovs-ofctl add-flow {0} cookie=0xDEFA,priority=60000,dl_type=0x0800,nw_src={1},nw_dst={2},actions="set_field:{3}->eth_dst,output:local"'.format(
            node.name, self.controller_ip, node.ip, '00:00:00:00:00:{:02x}'.format(node.id)))
        self.__set_default_parent_rules(node, node)

    def __set_default_parent_rules(self, for_node, current_node):
        parent = current_node.parent
        if parent is None:
            return
        print for_node.name, '<', current_node.name
        parent.sw.cmd('ovs-ofctl add-flow {0} cookie=0xDEFA,priority=60000,dl_type=0x0806,nw_dst={1},actions=output:{2}'.format(parent.name, for_node.ip, parent.neighbors[current_node]))
        parent.sw.cmd('ovs-ofctl add-flow {0} cookie=0xDEFA,priority=60000,dl_type=0x0800,nw_dst={1},actions=output:{2}'.format(parent.name, for_node.ip, parent.neighbors[current_node]))
        self.__set_default_parent_rules(for_node, parent)


def empty_net():
    "Create an empty network and add nodes to it."
    fileName = sys.argv[1]
    
    with open(fileName) as data_file:    
        data = json.load(data_file)
    
    nodes = set(data['nodes'])
    out_of_band_nodes = set(data['out_of_band_nodes'])
    edges = data['edges']

    mn = Mininet( topo=None, build=False)

    net = Network(mn)
    net.set_controller_node('controller', '10.0.0.1')

    net.read_data(nodes, edges, out_of_band_nodes)

    mn.start()

    call('ovs-vsctl set-fail-mode s100 standalone', shell=True)
    net.configure()

    CLI(mn)
    net.clear()
    mn.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    empty_net()
